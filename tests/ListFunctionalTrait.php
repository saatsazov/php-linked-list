<?php

namespace tests;

use Bagrat\PhpLinkedList\ListInterface;

/**
 * Verifies functional requirements of ListInterface implementation
 */
trait ListFunctionalTrait
{
    abstract public function getList(): ListInterface;

    public function testSorting()
    {
        $list = $this->getList();
        $list->add(5);
        $list->add(3);
        $list->add(34);
        $list->add(17);

        $this->assertEquals(
            [3, 5, 17, 34],
            $list->toArray()
        );
    }

    public function testValidRemoveByIndex()
    {
        $list = $this->getList();

        $list->add(3);
        $list->add(5);
        $list->add(17);
        $list->add(34);

        $list->removeByIndex(0);
        $this->assertEquals(
            [5, 17, 34],
            $list->toArray()
        );
        $this->assertEquals(
            3,
            $list->count()
        );

        $list->removeByIndex(1);
        $this->assertEquals(
            [5, 34],
            $list->toArray()
        );
        $this->assertEquals(
            2,
            $list->count()
        );

        $list->removeByIndex(1);
        $this->assertEquals(
            [5],
            $list->toArray()
        );

        $this->assertEquals(
            1,
            $list->count()
        );

        $list->removeByIndex(0);
        $this->assertEquals(
            [],
            $list->toArray()
        );
        $this->assertEquals(
            0,
            $list->count()
        );
    }

    public function testValidRemoveByValue()
    {
        $list = $this->getList();

        $list->add(3);
        $list->add(5);
        $list->add(17);
        $list->add(34);

        $list->removeByValue(3);
        $this->assertEquals(
            [5, 17, 34],
            $list->toArray()
        );

        $list->removeByValue(17);
        $this->assertEquals(
            [5, 34],
            $list->toArray()
        );

        $list->removeByValue(34);
        $this->assertEquals(
            [5],
            $list->toArray()
        );
    }

    public function testWithBigData()
    {
        $list = $this->getList();

        $verificationData = [];
        $count = 10_000;
        for ($i = 0; $i < $count; $i++) {
            $value = rand();

            $list->add($value);
            $verificationData[] = $value;
        }

        sort($verificationData);

        $this->assertEquals(
            $verificationData,
            $list->toArray()
        );
    }
}
