<?php

namespace tests;

use Bagrat\PhpLinkedList\LinkedList;
use Bagrat\PhpLinkedList\ListInterface;
use PHPUnit\Framework\TestCase;

class LinkedListTest extends TestCase
{
    use ListFunctionalTrait;

    public function getList(): ListInterface
    {
        return new LinkedList();
    }
}
