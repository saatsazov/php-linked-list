<?php

namespace Bagrat\PhpLinkedList;

use OutOfBoundsException;

/**
 * Arbitrary specified interface
 */
interface ListInterface
{
    /**
     * @return int[]
     */
    function toArray(): array;

    function add(int $value): void;
    function count(): int;

    /**
     * @throws OutOfBoundsException
     */
    function removeByIndex(int $id): void;

    /**
     * @throws UnexpectedValueException
     */
    function removeByValue(int $value): void;
}
