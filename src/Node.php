<?php

namespace Bagrat\PhpLinkedList;

class Node
{
    public int $value;
    public ?Node $next = null;
}
