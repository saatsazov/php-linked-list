<?php

namespace Bagrat\PhpLinkedList;

use OutOfBoundsException;
use UnexpectedValueException;

class LinkedList implements ListInterface
{
    protected ?Node $root = null;

    public function count(): int
    {
        $count = 0;

        $currentPointer = $this->root;
        while ($currentPointer !== null) {
            $count++;
            $currentPointer = $currentPointer->next;
        }

        return $count;
    }

    public function add(int $value): void
    {
        $currentPointer = $this->root;
        $previousPointer = null;
        while ($currentPointer !== null) {
            if ($value <= $currentPointer->value) {
                break;
            }
            $previousPointer = $currentPointer;
            $currentPointer = $currentPointer->next;
        }

        $item = new Node();
        $item->value = $value;

        if ($previousPointer === null) {
            // beginning of the list
            $item->next = $currentPointer;
            $this->root = $item;
            return;
        }

        if ($currentPointer === null) {
            // end of the list
            $previousPointer->next = $item;
            return;
        }

        // middle of the list
        $item->next = $currentPointer;
        $previousPointer->next = $item;
    }

    public function removeByIndex(int $index): void
    {
        if ($index === 0) {
            $oldItem = $this->root;
            $this->root = $this->root->next;
            $oldItem->next = null;
            return;
        }
        $currentIndex = 0;
        $currentPointer = $this->root;
        /**
         * @var ?Node
         */
        $previousPointer = null;
        while ($currentPointer !== null) {

            if ($currentIndex == $index) {

                $oldItem = $currentPointer;
                $previousPointer->next = $currentPointer->next;
                $oldItem->next = null;
                return;
            }

            $currentIndex++;
            $previousPointer = $currentPointer;
            $currentPointer = $currentPointer->next;
        }

        throw new OutOfBoundsException("No item found at $index");
    }

    public function removeByValue(int $value): void
    {
        if ($this->root?->value === $value) {
            $oldItem = $this->root;
            $this->root = $this->root->next;
            $oldItem->next = null;
            return;
        }
        $currentPointer = $this->root;
        /**
         * @var ?Node
         */
        $previousPointer = null;
        while ($currentPointer !== null) {

            if ($currentPointer->value == $value) {
                $oldItem = $currentPointer;
                $previousPointer->next = $currentPointer->next;
                $oldItem->next = null;
                return;
            }

            if ($currentPointer->value > $value) {
                break;
            }

            $previousPointer = $currentPointer;
            $currentPointer = $currentPointer->next;
        }

        throw new UnexpectedValueException("Item with value $value not found in the list");
    }

    public function toArray(): array
    {
        $result = [];

        $currentPointer = $this->root;
        while ($currentPointer !== null) {
            $result[] = $currentPointer->value;
            $currentPointer = $currentPointer->next;
        }

        return $result;
    }
}
