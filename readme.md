# Requirements 
"Implement a linkedlist that can store int values and keep them sorted"

My first thought was a tree. I'm not an expert in algorithms, but BinaryTree should work. The tree is linkedlist right?

Second thought was maybe it should be persistent and redis could be applied, but I just complicate things. Let's keep it simple.

To summarize:
- [x] prepare interface
- [x] write simple implementation
- [x] add tester, a series of test which will verify requirements. (It is actually a part of writing code in this case)
- [ ] add performance measurement? (memory, cpu)


# Run tests:
`vendor/bin/phpunit tests`


devlog: 
- I realize interface for linked list can be different. Do you need cache count? Which order to store? Should I write it with generic?
- Ok. It's called self-balancing tree and it's not that strait forward. Based on requirements if fast search needed, but I think the scope of this exercise is not writing super optimized algorithm.